var page = {
		pageNumber:1,
		pageCount:100,
		calls:[],
		target:{}
}
var EventUtil = {
		addHandler:function(element,type,handler){
			if(element.addEventListener){
				element.addEventListener(type,handler,false);
			}else if(element.attachEvent){
				element.attachEvent("on"+type,handler);
			}else{
				element["on"+type] = handler;
			}
		},
		removeHandler:function(element,type,handler){
			if(element.removeEventListener){
				element.removeEventListener(type,handler,false);
			}else if(element.detachEvent){
				element.detachEvent("on"+type,handler);
			}else{
				element["on"+type] = null;
			}
		}
}
page.bindEvent = function(){
	var contents = document.getElementsByName("page");
	for(var i = 0; i< contents.length; i++) {
        if(contents[i].className = 'page') {
        	page.target = contents[i];
        }
    }
	EventUtil.addHandler(page.target, "click", page.clickEventHandler);
};

page.clickEventHandler = function(event){
	
	var className = event.target.className;
	if(className =='page_num' || className == 'page_num more'){
		
		var pageNum = event.target.innerHTML;
		if(pageNum == '...'){
			pageNum = event.target.getAttribute("pageNum");
		}
		page.pageNumber = parseInt(pageNum);
		page.doCallBack();
		
	}else if(className == 'next_page'){
		
		if(page.pageNumber < page.pageCount){
			page.pageNumber++;
			page.doCallBack();
		}
		
	}else if(className == 'pre_page'){
		
		if(page.pageNumber > 1){
			page.pageNumber--;
			page.doCallBack();
		}
		
	}
}

page.refresh = function(pageNumber,pageCount){
	page.pageNumber = pageNumber;
	page.pageCount = pageCount;
	page.target.innerHTML = "";
	page.target.innerHTML = page.createPage();
}

page.createPage = function(){
	var html = [];
	
	html.push('<a class="page_num active">',page.pageNumber,'</a>');
	var i = page.pageNumber - 1;
	
	for(i;i > page.pageNumber - 2; i--){
		if(i >= 1){
			html.unshift('<a class="page_num">',i,'</a>');
		}
	}
	if(i>=1){
		html.unshift('<a class="page_num more" pageNum=',i,'>...</a>');
		html.unshift('<a class="page_num">',1,'</a>');
	}
	
	var j =  page.pageNumber + 1;
	for(j;j <= page.pageNumber + 1; j++){
		if(j <= page.pageCount){
			html.push('<a class="page_num">',j,'</a>');
		}
	}
	
	if(j <= page.pageCount)
	{
		html.push('<a class="page_num more" pageNum=',j,'>...</a>');
		html.push('<a class="page_num">',page.pageCount,'</a>');
	}
	
	html.unshift('<a class="pre_page">上一页</a>');
	html.push('<a class="next_page">下一页</a>');
	return html.join("");
}

page.doCallBack = function(){
	
	if(page.calls.length == 1 && window[page.calls[0]]){
		window[page.calls[0]]();
	}else if(page.calls.length == 2 && window[page.calls[0]][page.calls[1]]){
		window[page.calls[0]][page.calls[1]]();
	}else if (page.calls.length == 3 && window[page.calls[0]][page.calls[1]][page.calls[2]])
    {
        window[page.calls[0]][page.calls[1]][page.calls[2]]();
    }
	
}

page.init = function(callback){
	page.bindEvent();
	if(callback){
		page.calls = callback.split('.');
	}
}
